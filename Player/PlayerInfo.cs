﻿using System;
using System.Collections.Generic;
using UnityEngine;
using NewGame.Skill;
using NewGame.CharacterState;
using NewGame.Damage;

public class PlayerInfo : MonoBehaviour {
    
    [SerializeField]
    private Transform core;               // コアオブジェクト
    [SerializeField]
    private Transform effceStartPoint;    // エフェクト生成箇所

    private PlayerController playerController;
    private PlayerPanelInfo playerPanelInfo;

    private float maxHp;        // 最大HP
    private float nowHp;        // 現在HP
    private float maxMp;        // 最大MP
    private float nowMp;        // 現在MP

    private List<String> buffNameList = new List<String>();         // バフを名前で管理（TODO:未検証）
    private List<String> debuffNameList = new List<String>();       // デバフを名前で管理（TODO:未検証）
    private List<SkillData> useSkillDataList;                       // 使用可能スキルリスト
    private EffectData selectSkillEffectData = new EffectData();    // 選択されているスキルのエフェクトデータ

    private bool deathFlg = false;
    private bool changeSkillFlg = false;

    public float SkillRange {
        get { return selectSkillEffectData.skillData.range; }
    }

    public float HpRatio {
        get { return (nowHp / maxHp) * 100f; }
    }

    public EffectData EffectData {
        get { return selectSkillEffectData; }
    }

    public bool ChangeSkill {
        set { changeSkillFlg = value; }
        get { return changeSkillFlg; }
    }

    public bool Buff {
        get { return SkillType.BUFF.Equals(selectSkillEffectData.skillData.type); }
    }
    
    public bool RangeAttack {
        get { return SkillType.RANGE_ATTACK.Equals(selectSkillEffectData.skillData.type); }
    }

    public bool Death {
        get { return deathFlg; }
    }

    public Transform Core {
        get { return core; }
    }

    public int SkillNoListIndex {
        get { return useSkillDataList.IndexOf(selectSkillEffectData.skillData); }
    }
    
    void Awake() {
        StatusInit();

        List<int> useSkillNoList = new List<int> { 1, 2, 3, 4, 5 };
        useSkillDataList = SkillDataManager.GetTargetSkillList(useSkillNoList, true);
        SetEffectData(useSkillDataList[0]);

        playerPanelInfo = GameObject.FindGameObjectWithTag("PlayerPanel").GetComponent<PlayerPanelInfo>();
        playerController = GetComponent<PlayerController>();
    }

    void Start() {
        SetPlayerPanelHp();
    }
	
	void FixedUpdate() {
        // HPが0になった場合
        if (nowHp <= 0 && !deathFlg) {
            deathFlg = true;
            playerController.RequestState(PlayerState.DEATH);
        }
	}

    // ステータス初期化
    private void StatusInit() {
        // とりあえず適当値
        maxHp = 9000.0f;
        nowHp = 9000.0f;

        maxMp = 9000.0f;
        nowMp = 9000.0f;
    }

    // ダメージ計算
    public void DamageCalculation(float damage) {
        // ダメージ計算結果を現在HPにセット
        nowHp = DamageProcess.Process(nowHp, damage);

        SetPlayerPanelHp();
    }

    // パネルのHPを更新
    private void SetPlayerPanelHp() {
        playerPanelInfo.SetHp(HpRatio);
    }

    public void ChangeSkillData(int index) {
        // 使用可能スキルデータからデータを取得し、エフェクトデータをセット
        SetEffectData(useSkillDataList[index]);
    }

    public bool CheckSelectSkill(int no) {
        return selectSkillEffectData.skillData.no == no;
    }

    private void SetEffectData(SkillData data) {

        selectSkillEffectData.skillData = data;

        // バフの場合
        if (SkillType.BUFF.Equals(data.type)) {
            // コアを開始位置とする
            selectSkillEffectData.startPoint = core;
        } else {
            selectSkillEffectData.startPoint = effceStartPoint;
        }
    }

    public void SetEffectData(Transform startPoint) {
        selectSkillEffectData.startPoint = startPoint;
    }

    public void SetEffectData(Vector3 goalPosition) {
        selectSkillEffectData.goalPosition = goalPosition;
    }

    public void SetEffectData(Vector3 goalPosition, Transform startPoint) {
        selectSkillEffectData.goalPosition = goalPosition;
        selectSkillEffectData.startPoint = startPoint;
    }

    public void SetBuff() {
        // バフリストにスキル名を追加
        // TODO:とりあえずスキル名で管理してみる
        buffNameList.Add(selectSkillEffectData.skillData.name);

        // アイコン作成
        playerPanelInfo.CreateBuffIcon(selectSkillEffectData.skillData);
    }
}
