﻿using UnityEngine;
using NewGame.CharacterState;

// 攻撃が当たった場合の処理（プレイヤー用）
public class AttackHitProcessForPlayer : MonoBehaviour {

    private PlayerInfo playerInfo = null;
    private PlayerController playerController = null;

    private GameObject text;

    void Awake() {
        playerInfo = GetComponent<PlayerInfo>();
        playerController = GetComponent<PlayerController>();

        text = (GameObject)Resources.Load("Prefabs/Text/DamageText");
    }

    // オブジェクトが触れた場合の処理
    void OnCollisionEnter(Collision coll) {

        GameObject obj = coll.gameObject;

        // 敵のスキルが衝突した場合
        if (obj.tag == "EnemySkillEffect") {

            playerController.RequestState(PlayerState.DAMAGE, new Vector3(obj.transform.position.x, 0, 0));

            // 衝突したスキル情報を取得
            SkillEffectInfo skillInfo = obj.GetComponent<SkillEffectInfo>();

            // ダメージ計算
            playerInfo.DamageCalculation(skillInfo.Power);

            // ダメージテキスト生成
            GameObject textObj = Instantiate(text,
                playerInfo.Core.position,
                Quaternion.identity);
            textObj.GetComponent<DamageText>().SetDamageText(skillInfo.Power);
        }

    }
}
