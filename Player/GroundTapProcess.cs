﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class GroundTapProcess : MonoBehaviour, IPointerClickHandler {

    private PlayerInfo playerInfo;
    private BattleManager battleManager;

    void Awake() {
        playerInfo = GameObject.FindGameObjectWithTag("Player").GetComponent<PlayerInfo>();
        battleManager = GameObject.FindGameObjectWithTag("Manager").GetComponent<BattleManager>();
    }

    // アタッチされたオブジェクトがタップされた時の処理
    public void OnPointerClick(PointerEventData eventData) {
        // 敵のパネルを非表示
        //EnemyPanelInfo.Instance.PanelOff();

        battleManager.PlayerButtleProcess(eventData.pointerPressRaycast.worldPosition);
    }
}
