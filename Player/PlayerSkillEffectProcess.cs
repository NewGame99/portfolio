﻿using UnityEngine;
using NewGame.Skill;
using System.Collections;

public class PlayerSkillEffectProcess : MonoBehaviour {

    public void CreateEffet(EffectData effectData) {

        switch (effectData.skillData.no) {
            case 1: StartCoroutine(Raitoningu(effectData)); break;        // ライトニング
            case 2: StartCoroutine(RaitoninguZaRei(effectData)); break;   // ライトニング・ザ・レイ
            case 3: StartCoroutine(Baria(effectData)); break;             // バリア
            case 4: StartCoroutine(RaitoninguZaRei(effectData)); break;   // ライトニング・ザ・レイ
            case 5: StartCoroutine(RaitoninguZaRei(effectData)); break;   // ライトニング・ザ・レイ
            case 6: StartCoroutine(RaitoninguZaRei(effectData)); break;   // ライトニング・ザ・レイ
            case 7: StartCoroutine(RaitoninguZaRei(effectData)); break;   // ライトニング・ザ・レイ
        };
    }
    
    public IEnumerator Raitoningu(EffectData effectData) {
        /* 
         * 【スキル仕様】
         * タップ座標または敵に向かってエフェクトが放出される
         */

        yield return new WaitForSeconds(0.4f);
        CreateEffect(effectData, effectData.startPoint.position, Quaternion.identity);
    }

    public IEnumerator RaitoninguZaRei(EffectData effectData) {
        /* 
         * 【スキル仕様】
         * タップ座標または敵を基点とし複数のエフェクトを落下させる
         */

        yield return new WaitForSeconds(0.4f);

        Vector3 basePosition = effectData.goalPosition;

        for (int i = 0; i < 10; i++) {
            Vector3 startPosition = basePosition
                + new Vector3(
                    Random.Range(-2.0f, 1.0f),
                    10.0f,
                    Random.Range(-2.0f, 1.0f));

            CreateEffect(effectData, startPosition, Quaternion.identity);
            yield return new WaitForSeconds(0.4f);
        }
    }

    public IEnumerator Baria(EffectData effectData) {
        /* 
         * 【スキル仕様】
         * 自身にバフをかける
         */
        yield return new WaitForSeconds(0.4f);
        CreateEffect(effectData, effectData.startPoint.position, Quaternion.identity);
    }

    // エフェクトを生成
    public void CreateEffect(EffectData effectData, Vector3 startPosition, Quaternion rotation) {

        GameObject effect = Instantiate(effectData.skillData.effect,
            startPosition,
            rotation);

        if (!SkillType.BUFF.Equals(effectData.skillData.type)) {
            effect.GetComponent<SkillEffectInfo>().SetEffectData(effectData.skillData);
            effect.GetComponent<EffectProcess>().StartProcess(effectData);
        }
    }
}
